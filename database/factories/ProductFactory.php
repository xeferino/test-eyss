<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->sentence(1),
        'description' => $faker->text(),
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 2, $max = 100),
        'status' => '1'
        
    ];
});
