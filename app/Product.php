<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'categories_id', 'description', 'price', 'status'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'categories_id','id');
    }
}
