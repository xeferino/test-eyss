<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Test - Eyss</title>
    <style>
        .loader-page {
            position: fixed;
            z-index: 25000;
            background: rgb(255, 255, 255);
            left: 0px;
            top: 0px;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            transition:all .3s ease;
        }

        .loader-page::before {
                content: "";
                position: absolute;
                border: 2px solid rgb(50, 150, 176);
                width: 60px;
                height: 60px;
                border-radius: 50%;
                box-sizing: border-box;
                border-left: 2px solid rgba(50, 150, 176,0);
                border-top: 2px solid rgba(50, 150, 176,0);
                animation: rotarload 1s linear infinite;
                transform: rotate(0deg);
        }

        @keyframes rotarload {
            0%   {transform: rotate(0deg)}
            100% {transform: rotate(360deg)}
        }

        .loader-page::after {
            content: "";
            position: absolute;
            border: 2px solid rgba(50, 150, 176,.5);
            width: 60px;
            height: 60px;
            border-radius: 50%;
            box-sizing: border-box;
            border-left: 2px solid rgba(50, 150, 176, 0);
            border-top: 2px solid rgba(50, 150, 176, 0);
            animation: rotarload 1s ease-out infinite;
            transform: rotate(0deg);
        }

    </style>

  </head>
  <body>
    <div class="container">
        <div class="loader-page" id="loader-page"></div>
        <div id="categories"></div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"></script>
    
  </body>
</html>

<script>
    const CSRF = '{{ csrf_token() }}';
    const URL  = '{{ url("/") }}';

    axios.get(URL+"/categories")
    .then(response => {
        setTimeout(function () { document.getElementById("loader-page").hidden = true; }, 1000);
        document.getElementById("categories").innerHTML = response.data;
        console.log(this.categories);
    })
    .catch(e => {
        console.log(e);
    });
    

</script>